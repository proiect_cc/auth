const express = require('express');
const nodemailer = require('nodemailer')
const jwt = require('jsonwebtoken');

const { sendRequest } = require("../http-client");
const { validateFields } = require('../utils');
const { authorizeAndExtractToken } = require('../security/Jwt');
const { authorizeRoles } = require('../security/Roles');
const { hash,compare } = require('../security/Password');
const { generateToken } = require('../security/Jwt');

const {  ServerError } = require('../errors');

const router = express.Router();

const transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'diana.brodoceanu.97@gmail.com',
    pass: 'WalzerOpus69NR.2'
  }
})

const EMAIL_SECRET = 'qwertyuioasdfghjklzxcvbnm,.2345sdfghwertyuio3456789xdcfg'

// authorisation for other services (api, messages)
router.get('/authorize', authorizeAndExtractToken, authorizeRoles(), async (req, res, next) => {
  try {
    console.log("auth a primit request de la api")
    res.json({check: "OK"}).status(200).end();
  } catch(err) {
      next(err);
  }
});

// authorisation for other services (api, messages)
router.post('/authorize', authorizeAndExtractToken, authorizeRoles(), async (req, res, next) => {
  try {
    console.log("auth a primit request de la api")
    res.json({check: "OK"}).status(200).end();
  } catch(err) {
      next(err);
  }
});

// authorisation for other services (api, messages)
router.put('/authorize', authorizeAndExtractToken, authorizeRoles(), async (req, res, next) => {
  try {
    console.log("auth a primit request de la api")
    res.json({check: "OK"}).status(200).end();
  } catch(err) {
      next(err);
  }
});

router.post('/register', async (req, res, next) => {
    const {
        username,
        password,
        email,
        address,
        phoneNumber,
        firstName,
        lastName,
    } = req.body;

    // validare de campuri
    try {
        const minimumFields = {
            username: {
                value: username,
                type: 'ascii'
            },
            password: {
                value: password,
                type: 'ascii'
            }
        };

        const moreFields = {
          email: {
              value: email,
              type: 'ascii'
          },
          address: {
              value: address,
              type: 'ascii'
          },
          phoneNumber: {
              value: phoneNumber,
              type: 'ascii'
          },
          firstName: {
              value: firstName,
              type: 'ascii'
          },
          lastName: {
              value: lastName,
              type: 'ascii'
          }
        }

        var fieldsToBeValidated = {}
        if(username != 'admin' && username != 'suport'){
          fieldsToBeValidated = Object.assign(minimumFields, moreFields);
        }
        validateFields(fieldsToBeValidated);

        //pass request to IO to insert into db
        console.info(`Forwarding request to io-service ...`);
        console.log(`.. to ${process.env.IO_SERVICE_API_ROUTE}/users/register`)

        const hashedPassword = await hash(password);
        const ioRequest = {
          url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/register`,
          method: "POST",
          data: {...req.body, password: hashedPassword},
          headers: req.headers
        };
        const id = await sendRequest(ioRequest);

        // send confirmation email to new user mailbox
        // jwt.sign(
        //   {
        //     user: id
        //   },
        //   EMAIL_SECRET,
        //   {
        //     expiresIn: '1d'
        //   },
        //   (err, emailToken) => {
        //     var url = `http://192.168.100.4/api/v1/users/confirmation/${emailToken}`
        //     console.log(`process.env.STYLE: ${process.env.STYLE}`)
        //     if(process.env.STYLE == 'kong'){
        //       url = `http://127.0.0.1/auth/confirmation/${emailToken}`
        //     }
        //
        //     transporter.sendMail({
        //       to: email,
        //       subject: 'Confirm Email',
        //       html: `Please click this link to confirm your email: <a href="${url}"> ${url}</a>`
        //     });
        //   },
        // );
        res.status(201).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.post('/login', async (req, res, next) => {
  const {
      username,
      password
  } = req.body;

  try {
    const fieldsToBeValidated = {
        username: {
            value: username,
            type: 'ascii'
        },
        password: {
            value: password,
            type: 'ascii'
        }
    };

    validateFields(fieldsToBeValidated);

    //pass request to IO
    console.info(`Forwarding request to io-service ...`);
    console.log(`.. to ${process.env.IO_SERVICE_API_ROUTE}/users/login`)

    const ioRequest = {
      url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/login`,
      method: "POST",
      data: req.body,
      headers: req.headers
    };
    const user = await sendRequest(ioRequest);

    if (user === null) {
        throw new ServerError(`Utilizatorul inregistrat cu ${username} nu exista!`, 404);
    }

    if (user.confirmed == 'false'){
        throw new ServerError(`Please confirm your account to login!`, 404);
    }

    var token = null
    if (await compare(password, user.password)) {
        token = await generateToken({
            userId: user._id,
            userRole: user.role,
            userName: user.username,
        });
    } else {
      throw new ServerError("Combinatia de username si parola nu este buna!", 404);
    }

    res.status(200).json(token);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

// confirm new made account
router.get('/confirmation/:token', async (req, res, next) => {
  try{
    const jwtOutput = jwt.verify(req.params.token, EMAIL_SECRET)
    const { user: {id} } = jwtOutput
    console.log(`########### print {id} => ${jwtOutput}`)
    console.log(`########### print {id} => ${jwtOutput.user}`)

    // await UsersService.updateByUserId(jwtOutput.user, confirmed)
    //pass request to IO
    console.info(`Forwarding request to io-service ...`);
    console.log(`.. to ${process.env.IO_SERVICE_API_ROUTE}/users/confirmation/${jwtOutput.user}`)

    const ioRequest = {
      url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/confirmation/${jwtOutput.user}`,
      method: "GET",
      headers: req.headers
    };
    const ioAnswer = await sendRequest(ioRequest);
    console.log(ioAnswer)
    res.status(200).end()
  }catch(err){
    next(err);
  }
   console.log('########### before redirect')
  // return res.redirect('http://localhost:3004/#/login')
  // return res.redirect('http://localhost:3004/#/login')
  return res.status(200).end()
});

// get all users
router.get('/', async (req, res, next) => {
    try {

        //pass request to IO
        console.info(`Forwarding request to io-service ...`);
        console.log(`.. to ${process.env.IO_SERVICE_API_ROUTE}/users`)

        const ioRequest = {
          url: `http://${process.env.IO_SERVICE_API_ROUTE}/users`,
          method: "GET",
          headers: req.headers
        };
        const users = await sendRequest(ioRequest);
        res.json(users);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

// get user by id
router.get('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
  const {
    id
  } = req.params;
  try {

    validateFields({
      id: {
        value: id,
        type: 'ascii'
      }
    });
    //const user = await UsersService.getById(id);
    //pass request to IO
    console.info(`Forwarding request to io-service ...`);
    console.log(`.. to ${process.env.IO_SERVICE_API_ROUTE}/users/${id}`)

    const ioRequest = {
      url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/${id}`,
      method: "GET",
      headers: req.headers
    };
    const user = await sendRequest(ioRequest);
    res.json(user);
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});

router.put('/:id', authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
  const {
    id
  } = req.params;
  const {
      username,
      password,
      email,
      address,
      phoneNumber,
      firstName,
      lastName,
  } = req.body;

  // validare de campuri
  try {
      const minimumFields = {
          username: {
              value: username,
              type: 'ascii'
          },
          password: {
              value: password,
              type: 'ascii'
          }
      };

      const moreFields = {
        email: {
            value: email,
            type: 'ascii'
        },
        address: {
            value: address,
            type: 'ascii'
        },
        phoneNumber: {
            value: phoneNumber,
            type: 'ascii'
        },
        firstName: {
            value: firstName,
            type: 'ascii'
        },
        lastName: {
            value: lastName,
            type: 'ascii'
        }
      }
    var fieldsToBeValidated = {}
    if(username != 'admin' && username != 'suport'){
      fieldsToBeValidated = Object.assign(minimumFields, moreFields);
    }
    validateFields(fieldsToBeValidated);

    // await UsersService.updateById(id, username, password, email, address, phoneNumber, firstName, lastName);
    console.info(`Forwarding request to io-service ...`);
    console.log(`.. to ${process.env.IO_SERVICE_API_ROUTE}/users/${id}`)

    const ioRequest = {
      url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/${id}`,
      method: "PUT",
      data: req.body,
      headers: req.headers
    };
    const ioAnswer = await sendRequest(ioRequest);
    res.status(204).end();
  } catch (err) {
    // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
    next(err);
  }
});
//delete user by id
router.delete('/:id', authorizeAndExtractToken, authorizeRoles('admin','user'), async (req, res, next) => {
    const {
        id
    } = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        // se poate modifica
        //await UsersService.deleteById(id);
        console.info(`Forwarding request to io-service ...`);
        console.log(`.. to ${process.env.IO_SERVICE_API_ROUTE}/users/${id}`)

        const ioRequest = {
          url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/${id}`,
          method: "DELETE",
          headers: req.headers
        };
        const ioAnswer = await sendRequest(ioRequest);
        //await RecipesService.deleteRecipesByUserId(id)
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.delete('/',  authorizeAndExtractToken, authorizeRoles('admin','user'), async (req, res, next) => {
  try {
      // do logic
      //await UsersService.deleteAll();
      console.info(`Forwarding request to io-service ...`);
      console.log(`.. to ${process.env.IO_SERVICE_API_ROUTE}/users`)

      const ioRequest = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users`,
        method: "DELETE",
        headers: req.headers
      };
      const user = await sendRequest(ioRequest);
      return res.status(200).end();
  } catch (err) {
      // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
      next(err);
  }
});

module.exports = router;
