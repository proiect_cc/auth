const {
    ServerError
} = require('../../errors');

const authorizeRoles = (...rol) => {
    return (req, res, next) => {
        // console.log(req.headers)
        var roles = rol
        if(req.headers.roles){
          var roles = req.headers.roles.split(", ");
        }
        // console.log(rol)
        console.log(roles)
        console.log(req.state.decoded.userRole)
        for (let i = 0; i < roles.length; i++) {
            // console.log(roles[i])
            if (req.state.decoded.userRole === roles[i]) { //in req.state exista obiectul decoded trimis la middlewareul anterior, de autorizare token
                    return next();
            }
        }
        throw new ServerError('Nu sunteti autorizat sa accesati resursa!', 401);
    }
};

module.exports = {
    authorizeRoles
}
